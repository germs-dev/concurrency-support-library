- spurious notifications
- why is destructive interference diff so small?
- atomic_ref also has some extra features over a standard atomic
- trivially constructible -- test with trait
- https://en.cppreference.com/w/cpp/atomic/memory_order
- Concurrency chapter in Meyer book
- Example with two threads colliding and apart
- It can be informative to co-locate related data: keep them coupled.
- try_lock
- lock
- std::swap
- more shared_lock
- atomic_wait
- https://en.cppreference.com/w/cpp/header/stdatomic.h -- stdatomic.h.cxx_
- example of resetting a latch

```cpp
/*
Boolean specialisation that is guaranteed lock-free.
*/
#include <future>
TEST(atomic, atomic_flag) {

  std::atomic_flag lock = ATOMIC_FLAG_INIT;
  auto i = 0uz;

  auto f = std::async(std::launch::async, [&](){
		  atomic_flag_wait(&lock, false);

		  return true;
		  });

  // Check before we release the lock
  EXPECT_EQ(i, 0);

  lock.atomic_flag_notify_one();

		  // Wait for the future
  EXPECT_TRUE(f.get());

  EXPECT_EQ(i, 1);

}
```

```cpp
  // stops the execution of the current thread for a specified time duration
  // sleep_for
  //
  // stops the execution of the current thread until a specified time point
  // sleep_until
```
