#include "benchmark/benchmark.h"
#include <array>         // Fixed-sized array (size known at compile time)
#include <deque>         // vector of fixed-sized vectors
#include <forward_list>  // Singly linked list
#include <list>          // Doubly linked list
#include <map>           // RB tree
#include <set>           // RB tree
#include <unordered_map> // Hashmap
#include <unordered_set> // Hashmap
#include <valarray>      // Vector maths
#include <vector>        // Contiguous storage
// #include <flat_map>   // Hashmap with contiguous storage

// Create a chunk of data to benchmark with
constexpr auto X = [] {
  std::array<int, 16 * 1024uz> arr;
  int v = 0;

  for (auto &i : arr)
    i = v++;

  return arr;
}();

constexpr auto S = X.size();
constexpr auto num_elements = S;

// Front inserter
template <typename T> void front_inserter(benchmark::State &state) {
  for (auto _ : state)
    for (T x; const auto i : X)
      x.insert(x.begin(), i);
}

void insert_front_vector(benchmark::State &state) {
  front_inserter<std::vector<int>>(state);
}

BENCHMARK(insert_front_vector);

// Front pushers
template <typename T> void front_pusher(benchmark::State &state) {
  for (auto _ : state)
    for (T x; const auto i : X)
      x.push_front(i);
}

void push_front_deque(benchmark::State &state) {
  front_pusher<std::deque<int>>(state);
}

void push_front_list(benchmark::State &state) {
  front_pusher<std::list<int>>(state);
}

void push_front_forward_list(benchmark::State &state) {
  front_pusher<std::forward_list<int>>(state);
}

BENCHMARK(push_front_deque);
BENCHMARK(push_front_list);
BENCHMARK(push_front_forward_list);

template <typename T> void back_pusher(benchmark::State &state) {
  for (auto _ : state)
    for (T x; const auto i : X)
      x.push_back(i);
}

// Back pushers
void push_back_vector(benchmark::State &state) {
  back_pusher<std::vector<int>>(state);
}

void push_back_deque(benchmark::State &state) {
  back_pusher<std::deque<int>>(state);
}

void push_back_list(benchmark::State &state) {
  back_pusher<std::list<int>>(state);
}

BENCHMARK(push_back_vector);
BENCHMARK(push_back_deque);
BENCHMARK(push_back_list);

// Set inserters
template <typename T> void assoc_inserter(benchmark::State &state) {
  for (auto _ : state)
    for (T x; const auto i : X)
      x.insert(i);
}

void insert_set(benchmark::State &state) {
  assoc_inserter<std::set<int>>(state);
}

void insert_unordered_set(benchmark::State &state) {
  assoc_inserter<std::unordered_set<int>>(state);
}

BENCHMARK(insert_set);
BENCHMARK(insert_unordered_set);

// Set emplacer
template <typename T> void assoc_emplacer(benchmark::State &state) {
  for (auto _ : state)
    for (T x; const auto i : X)
      x.emplace(i);
}

void emplace_set(benchmark::State &state) {
  assoc_emplacer<std::set<int>>(state);
}

void emplace_unordered_set(benchmark::State &state) {
  assoc_emplacer<std::unordered_set<int>>(state);
}

BENCHMARK(emplace_set);
BENCHMARK(emplace_unordered_set);

// Map inserters
template <typename T> void assoc_inserter2(benchmark::State &state) {
  for (auto _ : state)
    for (T x; const auto i : X)
      x.emplace(i, i);
}

void map_insert(benchmark::State &state) {
  assoc_inserter2<std::map<int, int>>(state);
}

void unordered_map_insert(benchmark::State &state) {
  assoc_inserter2<std::unordered_map<int, int>>(state);
}

BENCHMARK(map_insert);
BENCHMARK(unordered_map_insert);

// Writing to containers
template <typename T>
void populate_sequence_container(benchmark::State &state) {
  for (auto _ : state)
    for (T x(S); const auto i : X)
      x[i] = i;
}

void populate_vector(benchmark::State &state) {
  populate_sequence_container<std::vector<int>>(state);
}

void populate_valarray(benchmark::State &state) {
  populate_sequence_container<std::valarray<int>>(state);
}

void populate_array(benchmark::State &state) {
  for (auto _ : state)
    for (std::array<int, num_elements> x; const auto i : X)
      x[i] = i;
}

BENCHMARK(populate_vector);
BENCHMARK(populate_array);
BENCHMARK(populate_valarray);

// Compile time containers
