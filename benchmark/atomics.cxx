#include "benchmark/benchmark.h"
#include <atomic>
#include <mutex>

void variable_increment_unguarded(benchmark::State &state) {
  size_t i = 0uz;
  for (auto _ : state)
    ++i;
}

void variable_increment_with_atomic(benchmark::State &state) {
  std::atomic<size_t> i = 0uz;
  for (auto _ : state)
    ++i;
}

void variable_increment_with_mutex(benchmark::State &state) {
  size_t i = 0uz;
  std::mutex m;

  for (auto _ : state) {
    m.lock();
    ++i;
    m.unlock();
  }
}

void variable_increment_with_scoped_lock(benchmark::State &state) {
  size_t i = 0uz;
  std::mutex m;
  for (auto _ : state) {

    std::scoped_lock lock(m);
    ++i;
  }
}

BENCHMARK(variable_increment_unguarded);
BENCHMARK(variable_increment_with_atomic);
BENCHMARK(variable_increment_with_mutex);
BENCHMARK(variable_increment_with_scoped_lock);
