#include "benchmark/benchmark.h"
#include <chrono>
#include <future>
#include <thread>

// Simple test function
namespace {
const auto func = [] { return true; };
}

// Run the func via different thread classes
void thread_async(benchmark::State &state) {
  for (auto _ : state) {
    auto f = std::async(std::launch::async, func);
    f.get();
  }
}

void thread_thread(benchmark::State &state) {
  for (auto _ : state) {
    std::thread t(func);
    t.join();
  }
}

void thread_jthread(benchmark::State &state) {
  for (auto _ : state) {
    std::jthread t(func);
    t.join();
  }
}

BENCHMARK(thread_async);
BENCHMARK(thread_thread);
BENCHMARK(thread_jthread);

BENCHMARK(thread_async)->UseRealTime();
BENCHMARK(thread_thread)->UseRealTime();
BENCHMARK(thread_jthread)->UseRealTime();
