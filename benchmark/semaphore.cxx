#include "benchmark/benchmark.h"
#include <semaphore>

// Create threads with noop
void semaphore_acquire_release(benchmark::State &state) {

  // Create open semaphore
  auto sem = std::binary_semaphore{1};

  // Just acquire and release repeatedly
  for (auto _ : state) {
    sem.acquire();
    sem.release();
  }
}

BENCHMARK(semaphore_acquire_release);
