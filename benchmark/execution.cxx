#include "benchmark/benchmark.h"
#include <execution>
#include <numeric>

// Create a chunk of data to play with
const std::vector<int> big_vec = [] {
  std::vector<int> v(10000);
  std::iota(v.begin(), v.end(), 0);
  return v;
}();

void exec_seq(benchmark::State &state) {
  for (auto _ : state) [[maybe_unused]]
    const auto sum =
        std::reduce(std::execution::seq, big_vec.cbegin(), big_vec.cend());
}

void exec_par(benchmark::State &state) {
  for (auto _ : state) [[maybe_unused]]
    const auto sum =
        std::reduce(std::execution::par, big_vec.cbegin(), big_vec.cend());
}

void exec_par_unseq(benchmark::State &state) {
  for (auto _ : state) [[maybe_unused]]
    const auto sum = std::reduce(std::execution::par_unseq, big_vec.cbegin(),
                                 big_vec.cend());
}

void exec_unseq(benchmark::State &state) {
  for (auto _ : state) [[maybe_unused]]
    const auto sum =
        std::reduce(std::execution::unseq, big_vec.cbegin(), big_vec.cend());
}

BENCHMARK(exec_seq);
BENCHMARK(exec_par);
BENCHMARK(exec_par_unseq);
BENCHMARK(exec_unseq);

BENCHMARK(exec_seq)->UseRealTime();
BENCHMARK(exec_par)->UseRealTime();
BENCHMARK(exec_par_unseq)->UseRealTime();
BENCHMARK(exec_unseq)->UseRealTime();
