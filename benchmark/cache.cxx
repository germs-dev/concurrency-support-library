#include "benchmark/benchmark.h"
#include <vector>

constexpr auto count = 128 * 1024uz;

void sum_stride1(benchmark::State &state) {
  std::vector<uint32_t> in(1024);
  for (auto _ : state)
    for (auto i = 0uz; i < count; ++i) {
      [[maybe_unused]] const auto sum1 = in[0] + in[1];
      [[maybe_unused]] const auto sum2 = in[0] + in[1];
    }
}
void sum_stride2(benchmark::State &state) {
  std::vector<uint32_t> in(1024);
  for (auto _ : state)
    for (auto i = 0uz; i < count; ++i) {
      [[maybe_unused]] const auto sum1 = in[0] + in[2];
      [[maybe_unused]] const auto sum2 = in[0] + in[1];
    }
}
void sum_stride3(benchmark::State &state) {
  std::vector<uint32_t> in(1024);
  for (auto _ : state)
    for (auto i = 0uz; i < count; ++i) {
      [[maybe_unused]] const auto sum1 = in[0] + in[3];
      [[maybe_unused]] const auto sum2 = in[0] + in[1];
    }
}
void sum_stride4(benchmark::State &state) {
  std::vector<uint32_t> in(1024);
  for (auto _ : state)
    for (auto i = 0uz; i < count; ++i) {
      [[maybe_unused]] const auto sum1 = in[0] + in[4];
      [[maybe_unused]] const auto sum2 = in[0] + in[1];
    }
}
void sum_stride15(benchmark::State &state) {
  std::vector<uint32_t> in(1024);
  for (auto _ : state)
    for (auto i = 0uz; i < count; ++i) {
      [[maybe_unused]] const auto sum1 = in[0] + in[15];
      [[maybe_unused]] const auto sum2 = in[0] + in[1];
    }
}
void sum_stride16(benchmark::State &state) {
  std::vector<uint32_t> in(1024);
  for (auto _ : state)
    for (auto i = 0uz; i < count; ++i) {
      [[maybe_unused]] const auto sum1 = in[0] + in[16];
      [[maybe_unused]] const auto sum2 = in[0] + in[1];
    }
}
void sum_stride31(benchmark::State &state) {
  std::vector<uint32_t> in(1024);
  for (auto _ : state)
    for (auto i = 0uz; i < count; ++i) {
      [[maybe_unused]] const auto sum1 = in[0] + in[31];
      [[maybe_unused]] const auto sum2 = in[0] + in[1];
    }
}
void sum_stride32(benchmark::State &state) {
  std::vector<uint32_t> in(1024);
  for (auto _ : state)
    for (auto i = 0uz; i < count; ++i) {
      [[maybe_unused]] const auto sum1 = in[0] + in[32];
      [[maybe_unused]] const auto sum2 = in[0] + in[1];
    }
}

BENCHMARK(sum_stride1);
BENCHMARK(sum_stride2);
BENCHMARK(sum_stride3);
BENCHMARK(sum_stride4);
BENCHMARK(sum_stride15);
BENCHMARK(sum_stride16);
BENCHMARK(sum_stride31);
BENCHMARK(sum_stride32);
