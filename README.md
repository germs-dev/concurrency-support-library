# C++ CONCURRENCY SUPPORT LIBRARY

_See my other projects: [turpin.dev](https://turpin.dev/)_

## Forward

> This is an exploration of the headers referenced in the cppreference.com [concurrency support library](https://en.cppreference.com/w/cpp/thread) documentation. The source is compiled, tested and benchmarked on each commit. Additionally, the C-style comments are converted to markdown prior to rendering this web page; this is purely to aid legibility: all documentation really is in [the code](https://gitlab.com/germs-dev/concurrency-support-library/) itself. Just for kicks -- and to keep repeat visits fresh -- the chapters are shuffled nightly.

Each source file can be run standalone in [Godbolt](https://godbolt.org/z/1coer6c8W).
```bash
g++-12 file.cxx -std=c++23 -O1 -lgtest -lgtest_main
```

### Further reading

- C++ High Performance: Master the art of optimizing the functioning of your C++ code, 2nd Edition -- Bjorn Andrist, Viktor Sehr, Ben Garney
- C++ Concurrency in Action, Second Edition -- Anthony Williams

___

