#include "gtest/gtest.h"
#include <barrier>
#include <thread>
/*
A barrier is a multi-use latch. It is released when enough threads are queued
up. Unlike a regular latch it also gives you the option of calling a routine
when the latch is released.

### `std::barrier`
*/
TEST(latch, barrier) {
  // Function to call when barrier opens
  auto finished = bool{false};
  const auto we_are_done = [&]() { finished = true; };

  // Initialise barrier with the number of threads
  std::barrier b(2, we_are_done);

  // Thread function
  const auto func = [&]() { b.arrive_and_wait(); };

  // Create our threads (note braces for scope)
  {
    std::jthread t1{func};
    std::jthread t2{func};
  }

  EXPECT_TRUE(finished);
}
