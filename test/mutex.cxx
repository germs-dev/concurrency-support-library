#include "gtest/gtest.h"
#include <mutex>
/*
A mutual exclusion lock is the starting point for all things concurrent,
offering a standard way to protect access a resource; but there are multiple
ways to unlock it.
*/

namespace {
int value{};
std::mutex mux{};
}; // namespace

/*
### `std::mutex`

To safely -- i.e., predictably -- update a value concurrently we must first lock
it. You can lock/unlock explicitly (below), but this can quickly go wrong if the
unlock is skipped: e.g, by bad logic or exceptions.
*/
TEST(mutex, mutex) {
  mux.lock();
  value = 1;
  mux.unlock();

  EXPECT_EQ(value, 1);
}

/*
### `std::lock_guard`

Missing an unlock may result in a deadlock, so the Standard
Library offers a few ways to mitigate this risk and unlock automatically using
the RAII paradigm. Multiple mutexes can be acquired safely using scoped locks.
*/
TEST(mutex, lock_guards) {
  // Basic locking of a single mutex.
  {
    std::lock_guard lock(mux);
    ++value;
    EXPECT_EQ(value, 2);
  }

  // Deadlock safe locking of one or more mutexes
  {
    std::mutex mux2;
    std::scoped_lock lock(mux, mux2);
    ++value;
    EXPECT_EQ(value, 3);
  }
}

/*
### `std::call_once`

This can be emulated with a static IIFE function, but "call once" does
express intention more directly.
*/
TEST(mutex, call_once) {

  // It's a bit of a shame you need this flag
  std::once_flag flag;
  auto i = size_t{0};

  std::call_once(flag, [&]() { ++i; });
  EXPECT_EQ(i, 1);

  std::call_once(flag, [&]() { ++i; });
  EXPECT_EQ(i, 1);

  std::call_once(flag, [&]() { ++i; });
  EXPECT_EQ(i, 1);
}
