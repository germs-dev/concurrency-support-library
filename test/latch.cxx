#include "gtest/gtest.h"
#include <latch>
#include <thread>
/*
A latch is a single-use synchronisation primitive; single-use meaning you have
to reset it. Really you could just always use a barrier which doesn't need
resetting.

### `std::latch`
*/
TEST(latch, latch) {
  // Initialise latch with the number of threads
  auto l = std::latch{2};

  // Variable to test how many threads have been allowed through the latch
  auto i = std::atomic{0uz};

  // Thread function waits for the others then updates a variable atomically
  const auto func = [&]() {
    l.arrive_and_wait();
    ++i;
  };

  // Create our threads
  auto t1 = std::thread{func};
  auto t2 = std::thread{func};

  // Remember to join
  t1.join();
  t2.join();

  EXPECT_EQ(i, 2);
}
