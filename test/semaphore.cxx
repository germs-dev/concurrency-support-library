#include "gtest/gtest.h"
#include <atomic>
#include <chrono>
#include <semaphore>
#include <thread>
/*
### `std::counting_semaphore`

Like `std::mutex` but you're saying you have multiple instances of a resource
available for concurrent access.
*/
TEST(semaphore, counting_semaphore) {
  // Initialise to 2 resources and make them both available
  auto sem = std::counting_semaphore<2>{2};
  auto i = std::atomic{0uz};

  // Grab one before starting the threads
  sem.acquire();

  // Create threads, the first will pass straight through
  std::jthread t1{[&] {
    sem.acquire();
    ++i;
  }};

  // This thread will block
  std::jthread t2{[&] {
    sem.acquire();
    ++i;
  }};

  // Check only one thread has updated the value
  std::this_thread::sleep_for(std::chrono::microseconds{1});
  EXPECT_EQ(i, 1);

  // Release the second thread
  sem.release();

  // Confirm it has changed afterwards
  std::this_thread::sleep_for(std::chrono::microseconds{1});
  EXPECT_EQ(i, 2);
}

/*
### `std::binary_semaphore`

A binary semaphore is just a specialisation of a counting semaphore. These are
equivalent:
```cpp
auto sem = std::binary_semaphore{1};
auto sem = std::counting_semaphore<1>{1};
```
Let's create one and make it available immediately.
*/
TEST(semaphore, binary_semaphore) {
  // Initialise semaphore to available
  auto sem = std::binary_semaphore{1};
  auto i = size_t{0};

  // Grab the semaphore before starting the thread
  sem.acquire();

  // Create thread and try semaphore
  std::jthread t{[&] {
    sem.acquire();
    ++i;
  }};

  // Check the thread is blocked
  std::this_thread::sleep_for(std::chrono::microseconds{1});
  EXPECT_EQ(i, 0);

  // Release the thread
  sem.release();

  // Confirm value has changed afterwards
  std::this_thread::sleep_for(std::chrono::microseconds{1});
  EXPECT_EQ(i, 1);
}
