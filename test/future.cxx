#include "gtest/gtest.h"
#include <future>
/*
`std::async` is a powerful way to handle return values from multiple threads,
think of it like pushing a calculation into the background. It executes a
function asynchronously and returns a `std::future` that will eventually hold
the result of that function call. Quite a nice way to reference the result of
calculation executed in another thread. Of course you must factor in the
overhead of actually creating the thread -- 20µs, say; but your mileage may
vary.

It's worth mentioning that a `std::future` is only moveable, but the Standard Library provides a _shared_ future; so you can start multiple threads and pass in a value you don't yet know.

### `std::async`
*/
TEST(thread, async) {
  // Calculate some things in the background
  auto a = std::async(std::launch::async, [] { return 1; });
  auto b = std::async(std::launch::async, [] { return 2; });
  auto c = std::async(std::launch::async, [] { return 3; });

  // Block until they're all satisfied
  const auto sum = a.get() + b.get() + c.get();

  EXPECT_EQ(sum, 6);
}
