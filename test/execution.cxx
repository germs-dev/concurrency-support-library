#include "gtest/gtest.h"
#include <execution>
#include <numeric>
/*
Many of the Standard Library algorithms can take an execution policy, which is
quite an exciting way to parallelise existing code. But remember it offers no
thread safety: you must still protect your data as you would for any other
threads. You also need to link against the TBB library.
*/

// Create a big (-ish) chunk of data to play with
static const std::vector<int> vec = [] {
  std::vector<int> v(10000);
  std::iota(begin(v), end(v), 0);
  return v;
}();

/*
Some algorithms also have an `_if` version that takes predicate: e.g.,
`std::replace` and `std::replace_if`.

1. `std::sort`
1. `std::copy`
1. `std::transform`
1. `std::accumulate`
1. `std::for_each`
1. `std::reduce`
1. `std::inclusive_scan`
1. `std::exclusive_scan`
1. `std::transform_reduce`
1. `std::remove`
1. `std::count`
1. `std::max_element`
1. `std::min_element`
1. `std::find`
1. `std::generate`

### `std::execution::par`
Let's test each policy, and confirm the sums are equal.

It must be noted, though, that you must not throw exceptions in these routines
(even for the sequential option) or else crash and burn (`std::terminate`.)
*/
TEST(thread, execution_policy) {

  // Sequential -- bof
  const auto s0 = std::reduce(std::execution::seq, begin(vec), cend(vec));

  // Parallel -- simple threads only
  const auto s1 = std::reduce(std::execution::par, cbegin(vec), cend(vec));

  // Parallel -- throw the whole tool shed at it
  const auto s2 =
      std::reduce(std::execution::par_unseq, cbegin(vec), cend(vec));

  // Parallel -- vectorisation only
  const auto s3 = std::reduce(std::execution::unseq, cbegin(vec), cend(vec));

  EXPECT_EQ(s0, s1);
  EXPECT_EQ(s0, s2);
  EXPECT_EQ(s0, s3);
}
