#include "gtest/gtest.h"
#include <new>
/*
You can query cache line size programmatically with these constants. Ensure data
used together by a single thread are co-located; and conversely, avoid _false_
sharing by keeping unrelated data apart.

### `std::hardware_destructive_interference_size`
*/
TEST(new, interference) {

  struct {
    alignas(std::hardware_destructive_interference_size) int x0;
    alignas(std::hardware_destructive_interference_size) int x1;
    alignas(std::hardware_destructive_interference_size) int x2;
  } together;

  struct {
    alignas(std::hardware_constructive_interference_size) int x0;
    alignas(std::hardware_constructive_interference_size) int x1;
    alignas(std::hardware_constructive_interference_size) int x2;
  } apart;

  EXPECT_GE(&together.x1 - &together.x0, 16);
  EXPECT_LT(&apart.x1 - &apart.x0, std::hardware_destructive_interference_size);
  EXPECT_EQ(std::hardware_destructive_interference_size, 64);
  EXPECT_EQ(std::hardware_constructive_interference_size, 64);
}
