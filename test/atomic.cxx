#include "gtest/gtest.h"
#include <atomic>
/*
Update a variable thread safely. Can be used with any built-in type, or in
fact, anything that is "trivially constructible".

### `std::atomic`
*/
TEST(atomic, atomic) {
  auto i = std::atomic{0uz};

  // This is thread safe
  ++i;

  EXPECT_EQ(i, 1);
}

/*
### `std::atomic_ref`

Wrap a non-atomic thing in atomic love.
*/
TEST(atomic, atomic_ref) {
  auto i = 0uz;
  auto ii = std::atomic_ref{i};

  // This is also thread safe
  ++ii;

  EXPECT_EQ(ii, i);
  EXPECT_EQ(ii, 1);
}
