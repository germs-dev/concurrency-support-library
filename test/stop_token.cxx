#include "gtest/gtest.h"
#include <semaphore>
#include <stop_token>
#include <thread>
/*
### `std::stop_token`

Built-in method to make a `jthread` stop.

Instruct the thread to exit its processing loop. I've used a semaphore to make
the calling thread wait for the processing thread to tidy up.
*/
TEST(thread, stop_token_explicit) {
  using namespace std::literals::chrono_literals;
  // Update this variable when we leave the stop token loop
  auto i = 0uz;

  // Create a semaphore to interact with the processing thread
  std::binary_semaphore sem{0};

  // Create processing thread
  std::jthread t{[&](std::stop_token stop_token) {
    // Do some work until told otherwise
    while (not stop_token.stop_requested())
      std::this_thread::sleep_for(1us);

    // Our work here is done, update variable
    ++i;

    // Tell the calling thread to continue
    sem.release();
  }};

  // Check variable hasn't been updated, the processing thread is in its main
  // loop at this point
  EXPECT_EQ(i, 0);

  // Tell the processing thread to stop
  t.request_stop();

  // Wait for it to finish
  sem.acquire();

  // Check variable has been updated
  EXPECT_EQ(i, 1);
}

/*
`std::jthread` also stops implicitly when the thread goes out of scope.
*/
TEST(thread, stop_token_implicit) {
  // Update this variable when we leave the stop token loop
  auto i = 0uz;

  // Create a semaphore to interact with the processing thread
  std::binary_semaphore sem{0};

  // Create thread, it stops when it goes out of scope
  {
    std::jthread t{[&](std::stop_token stop_token) {
      // Do some work until told otherwise
      while (not stop_token.stop_requested())
        std::this_thread::sleep_for(std::chrono::microseconds{1});

      // Our work here is done, update variable and poke the calling thread
      ++i;
      sem.release();
    }};

    // Check variable hasn't been updated, processing thread is doing its thing
    EXPECT_EQ(i, 0);

    // Thread goes out of scope and calls stop implicitly
  }

  // Check variable has been updated when processing thread signals
  sem.acquire();
  EXPECT_EQ(i, 1);
}
