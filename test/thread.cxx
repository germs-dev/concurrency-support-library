#include "gtest/gtest.h"
#include <algorithm>
#include <ranges>
#include <thread>
#include <vector>
/*
The go-to platform-independent thread API. It's been a lot easier since
`std::thread` was introduced in C++11.

### `std::thread`

You must call `join()` on the thread after creating it, otherwise bad
things. Typically this is managed using a vector of threads.
*/
TEST(thread, thread) {

  auto threads = std::vector<std::thread>{};

  // Create threads
  for ([[maybe_unused]] const auto _ : std::ranges::iota_view{1, 10})
    threads.emplace_back([] {});

  // Catch threads
  for (auto &t : threads)
    if (t.joinable())
      t.join();
}

/*
### `std::jthread`

A joining thread is the same as a regular thread but has an implicit `join()` in
the destructor. You can still join a `std::jthread`, of course, which can be a
convenient synchronisation point.
*/
TEST(thread, jthread) {

  {
    std::jthread t{[] {}};
  }

  // join() is called when it goes out of scope
}

/*
### `std::this_thread`

Useful functions within a thread.
*/
TEST(thread, functions) {

  // Suggest reschedule of threads
  std::this_thread::yield();

  // Get the ID of the current thread, useful for tracking/logging
  const auto id = std::this_thread::get_id();
  EXPECT_NE(id, std::thread::id{});
}
