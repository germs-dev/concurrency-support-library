#include "gtest/gtest.h"
#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>
/*
### `std::condition_variable`

Like a mutex but with an additional predicate.
*/
TEST(condition_variable, notify_one_with_predicate) {
  std::mutex m;
  std::condition_variable cv;
  std::queue<int> q;

  // Consumer thread waits for data to change
  std::jthread consumer{[&]() {
    std::unique_lock<std::mutex> lock(m);

    // Wait until the data change
    cv.wait(lock, [&] { return not q.empty(); });

    // Empty the queue
    while (not q.empty())
      q.pop();
  }};

  // Producer thread updates data
  std::jthread producer{[&]() {
    std::unique_lock<std::mutex> lock(m);
    q.push(0);
    q.push(1);
    q.push(2);

    EXPECT_FALSE(q.empty());

    // Notify the other thread that something has changed
    cv.notify_one();
  }};

  // You don't have to join a thread but it offers a convenient synchonrisation
  // point
  consumer.join();
  EXPECT_TRUE(q.empty());
}
